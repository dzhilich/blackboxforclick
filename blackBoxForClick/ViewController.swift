//
//  ViewController.swift
//  blackBoxForClick
//
//  Created by Dmytro Zhylich on 28.04.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var labelView: UILabel!
    @IBOutlet weak var boxView: UIView!
    
    var counter = 0
    
    var h = 0
    var w = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        labelView.text = String(counter)
        
        h = Int(view.frame.size.height)
        w = Int(view.frame.size.width)
        
        boxView.center = CGPoint(x: w/2, y: h/2)
        
        print("allsize:", h, w)
    }
    
    
    @IBAction func btnBoxAction(_ sender: UIButton) {
        let x = Int.random(in: 50..<w-50)
        let y = Int.random(in: 50..<h-50)

        print("New points:", x, y)

        boxView.center = CGPoint(x: x, y: y)

        counter += 1
        labelView.text = String(counter)
    }
}

